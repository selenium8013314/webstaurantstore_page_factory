package com.webstaurantstore_page_factory.tests;

import java.util.ArrayList;
import java.util.Arrays;

import com.webstaurantstore_page_factory.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import com.webstaurantstore_page_factory.pages.HomePage;
import com.webstaurantstore_page_factory.pages.SearchPage;
import com.webstaurantstore_page_factory.pages.CartPage;
import com.webstaurantstore_page_factory.listeners.ScreenshotListener;
import com.webstaurantstore_page_factory.data.DataProvider;

@Listeners(ScreenshotListener.class)
@Test(testName = "Webstaurantstore Search test", description = "Webstaurantstore search test")
public class SearchTablesTest extends BaseTest{

	WebDriver driver;
	BaseTest browser;
	HomePage homePage;
	SearchPage searchPage;
	CartPage cartPage;

	private static final String SEARCH_PAGE_TITLE = "Stainless Work Table - WebstaurantStore";
	private static final String TOKEN_EXPECTED = "Table";
	private static final String VIEW_ALERT = "1 item added to your cart";
	private static final String CART_PAGE_TITLE = "WebstaurantStore Cart";
	private static final int ITEM_QTY = 1;
	private static final String EMPTY_CART_BUTTON = "Empty Cart";
	private static final String EMPTY_CART_MESSAGE = "Your cart is empty.";

	public SearchTablesTest() {
		super();
	}

    @BeforeMethod
	public void setUp(ITestContext testContext) {

		browser = new BaseTest();

		driver = browser.startDriver(testContext);

		homePage = new HomePage(driver);
		searchPage = new SearchPage(driver);
		cartPage = new CartPage(driver);

		openUrl(driver);

	}

	@Test(priority = 2, dataProviderClass = DataProvider.class, dataProvider = "search", description = "Verify if all items contains the 'Table' word")
	public void verifyItems(String search) {
		homePage.searchProduct(search);

		String[] capitalizedSearch = Arrays.stream(search.split(" "))
				.map(word -> Character.toUpperCase(word.charAt(0)) + word.substring(1))
				.toArray(String[]::new);
		String titleStart = String.join(" ", capitalizedSearch);

		Assert.assertEquals(searchPage.getTitle(titleStart), SEARCH_PAGE_TITLE, "Title is different than " + SEARCH_PAGE_TITLE);

		int pageNo = 2;
		while (!searchPage.isLastPage()) {

			Assert.assertEquals(searchPage.getTitle(), SEARCH_PAGE_TITLE + " - Page " + pageNo,
					"Title is different than " + SEARCH_PAGE_TITLE + " - Page " + pageNo);

			ArrayList<String> searchResults = searchPage.searchResultByPage();

			for (String item : searchResults) {
				Assert.assertTrue(item.contains(TOKEN_EXPECTED),
						item + " does not contains " + TOKEN_EXPECTED + " word in the description");
			}
			pageNo++;
		}
	}

	@Test(priority = 3, dataProviderClass = DataProvider.class, dataProvider = "search", description = "Add the last item to the Cart")
    public void addItemToCart(String search) {
		homePage.searchProduct(search);

		searchPage.goToLastItemsPage();
		String itemAdded = this.searchPage.addLastItemToCart();

		Assert.assertEquals(searchPage.viewCartAlert(), VIEW_ALERT, "The view alert is not displayed");
		// Assert that the selected item is added to the cart
		Assert.assertEquals(itemAdded, searchPage.getItemAddedAlert(), itemAdded + " expected but found " +
				searchPage.getItemAddedAlert());

		// Submit the view cart button in the displayed alert after adding an item
		searchPage.submitViewCartButton();

		// Assert that the Cart Title is displayed
		Assert.assertEquals(cartPage.getTitle(), CART_PAGE_TITLE, "Title is different than " + CART_PAGE_TITLE);
		// Assert that item appeared in the cart
		Assert.assertTrue(cartPage.getItemAdded(itemAdded), itemAdded + " is not found in the cart");
		// Assert that number of items is equal to 1
		Assert.assertEquals(cartPage.getItemQty(), ITEM_QTY, "Expected qty for " + itemAdded + " is " +
				ITEM_QTY + " but found " + cartPage.getItemQty());
		// Assert that empty cart button is displayed
		Assert.assertEquals(cartPage.getEmptyCartButtonText(), EMPTY_CART_BUTTON, "Empty Cart button not found");
	}

	@Test(priority = 4, dataProviderClass = DataProvider.class, dataProvider = "search", description = "Empty Cart")
	public void emptyCart(String search) {
		homePage.searchProduct(search);

		String itemAdded = this.searchPage.addFirstItemToCart();
		System.out.println("Item added: " + itemAdded);

		homePage.openCart();

		// Assert that the Cart Title is displayed
		Assert.assertEquals(cartPage.getTitle(), CART_PAGE_TITLE, "Title is different than " + CART_PAGE_TITLE);
		// Assert that empty cart button is displayed
		Assert.assertEquals(cartPage.getEmptyCartButtonText(), EMPTY_CART_BUTTON, "Empty Cart button not found");

		// If EMPTY_CART tag doesn't exist, click on 'Empty Cart' button
		if (!cartPage.emptyCartMessage().equals(EMPTY_CART_MESSAGE)) {
			cartPage.clickEmptyCartButton();
			cartPage.confirmEmptyCart();
		}

		Assert.assertEquals(cartPage.emptyCartMessage(), EMPTY_CART_MESSAGE, EMPTY_CART_MESSAGE + " no found");
    }

	/*Test case: verify if a cart is already empty */
	@Test(priority = 1, description = "Validate if Cart is empty.")
    public void verifyIfCartIsEmpty() {
		homePage.openCart();

		// Assert that the Cart Title is displayed
		Assert.assertEquals(cartPage.getTitle(), CART_PAGE_TITLE, "Title is different than " + CART_PAGE_TITLE);
		// Assert if the cart is empty
		Assert.assertEquals(cartPage.emptyCartMessage(), EMPTY_CART_MESSAGE, EMPTY_CART_MESSAGE + " no found");
	}

	@AfterMethod
	public void teardown(ITestResult result) {
		org.apache.log4j.Logger logger = Logger.getLogger(BaseTest.class);

		if (result.getStatus() == ITestResult.FAILURE) {
			logger.info(result.getTestName() + " is Failed");
		}

		if (driver != null) {
			driver.quit();
		}
	}
}