package com.webstaurantstore_page_factory.tests;

import java.util.Map;
import java.util.logging.Level;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;

import org.testng.ITestContext;

public class BaseTest {
	
  protected WebDriver driver;
  private static final String HOME_PAGE_URL = "https://www.webstaurantstore.com/";

  protected void openUrl(WebDriver driver){
    driver.get(HOME_PAGE_URL);
  }

  protected WebDriver startDriver(ITestContext testContext) {
    String browserName = System.getProperty("browserName", "chrome"); // Default to Chrome

    if (browserName == null) {
      browserName = "chrome";
    }

    ChromeOptions chromeOptions = new ChromeOptions();
    FirefoxOptions firefoxOptions = new FirefoxOptions();
    EdgeOptions edgeOptions = new EdgeOptions();
    SafariOptions safariOptions = new SafariOptions();

    LoggingPreferences logPrefs = new LoggingPreferences();
    logPrefs.enable(LogType.PERFORMANCE, Level.ALL);

    if (browserName.startsWith("chrome")){
      chromeOptions.setCapability("goog:loggingPrefs", logPrefs);

      chromeOptions.addArguments("--disable-notifications", "--ignore-certificate-errors", "--disable-extensions");
      chromeOptions.addArguments("disable-infobars");
      chromeOptions.addArguments("--remote-debugging-port=9222");
      chromeOptions.addArguments("disable-dev-shm-usage");
      chromeOptions.addArguments("disable-features=VizDisplayCompositor");
      chromeOptions.addArguments("disable-features=IsolateOrigins,site-per-process");
      chromeOptions.addArguments("no-sandbox");
      chromeOptions.addArguments("disable-popup-blocking");
      chromeOptions.addArguments("enable-javascript");

      chromeOptions.setExperimentalOption("useAutomationExtension", false);
      chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
      chromeOptions.setExperimentalOption("prefs", Map.of(
              "profile.default_content_setting_values.notifications", 2,
              "profile.default_content_setting_values.geolocation", 1,
              "credentials_enable_service", false,
              "profile.password_manager_enabled", false
      ));

      if(browserName.equals("chrome_headless")){
        chromeOptions.addArguments("headless=new");
        chromeOptions.addArguments("force-device-scale-factor=0.6");
      }
    } else
    if (browserName.startsWith("edge")){
      edgeOptions.setCapability("moz:edgeOptions", logPrefs);

      edgeOptions.addArguments("--enable-logging");
      edgeOptions.addArguments("--log-level=3");  // Set log level to DEBUG
      edgeOptions.addArguments("--disable-notifications", "--ignore-certificate-errors", "--disable-extensions");
      edgeOptions.addArguments("disable-infobars");
      edgeOptions.addArguments("--remote-debugging-port=9222");
      edgeOptions.addArguments("disable-dev-shm-usage");
      edgeOptions.addArguments("disable-features=VizDisplayCompositor");
      edgeOptions.addArguments("disable-features=IsolateOrigins,site-per-process");

      edgeOptions.setExperimentalOption("useAutomationExtension", false);
      edgeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});

      if(browserName.equals("edge_headless")){
        edgeOptions.addArguments("headless=new");
        edgeOptions.addArguments("force-device-scale-factor=0.6");
      }
    } else
    if (browserName.startsWith("firefox")){
//       Verify why for the alert message they fail
//      firefoxOptions.setCapability("moz:firefoxOptions", logPrefs);

//      is the same as the above
//      firefoxOptions.addArguments("--log-level=all");
//      firefoxOptions.addArguments("--verbose");

      firefoxOptions.addArguments("--disable-notifications","--ignore-certificate-errors");
      firefoxOptions.addArguments("--disable-infobars");
      firefoxOptions.addArguments("--enable-site-per-process=false");

      if(browserName.equals("firefox_headless")){
        firefoxOptions.addArguments("--headless");
        firefoxOptions.addArguments("force-device-scale-factor=0.6");
      }
    } else
    if (browserName.equals("safari"))
    {
      safariOptions.setAutomaticProfiling(true);
      safariOptions.setAutomaticInspection(true);

// Following Capabilities are deprecated, verify which are the new ones:

//    safariOptions.setCapability("enableRemoteAutomation", true);
//    safariOptions.setCapability("safari.enableLogs", true);
//    safariOptions.setCapability("safari.enableRemoteDebugging", true);
//    safariOptions.setCapability("safari.remoteDebuggingPort", 9222);
//    safariOptions.setCapability("autoAcceptAlert", true); // Accept alerts automatically (adjust as needed)
//    safariOptions.setCapability("cleanSession", true); // Start a new session each time
//    safariOptions.setCapability("browserName", "safari");
//    safariOptions.setCapability("disable-features", "IsolateOrigins,site-per-process");
//      safariOptions.setCapability("safari.options.preferences",
//              "{\"WebKitPreferences\": {\"developerToolsEnabled\": false, \"javaScriptEnabled\": true}}");
//    safariOptions.setCapability("safari.options.preferences",
//            "{'WebKitPreferences': {'developerToolsEnabled': false, 'javaScriptEnabled': true}}");
//
//    safariOptions.setAutomaticInspection(false);
    }

    switch (browserName) {
      case "chrome", "chrome_headless":
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(chromeOptions);
        break;
        // Check if it automatically installs the latest ChromeVersion
//        case "edge":
      case "edge", "edge_headless":
        WebDriverManager.edgedriver().setup();
        driver = new EdgeDriver(edgeOptions);
        break;
//      case "firefox":
      case "firefox", "firefox_headless":
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver(firefoxOptions);
        break;
      case "safari":
        WebDriverManager.safaridriver().setup();
        driver = new SafariDriver(safariOptions);
        break;
      default:
        throw new IllegalArgumentException("Browser \"" + browserName + "\" isn't supported.");
    }

    testContext.setAttribute("webDriver", driver);
    driver.manage().window().maximize();
    driver.manage().deleteAllCookies();

    return driver;
  }
  
}
