package com.webstaurantstore_page_factory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.NoSuchElementException;

public class HomePage extends Page {
	@FindBy(id = "searchval")
	private WebElement searchBar;

	@FindBy(id="cartItemCountSpan")
	private WebElement cartButton;

	public HomePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	
	public void searchProduct(String search) {
		searchBar.clear();
		searchBar.sendKeys(search);
		searchBar.submit();
	}
	
	
	public void openCart() {
		
		try {
			
			if (cartButton != null) {
				cartButton.click();
			}
		
		}
		catch (NoSuchElementException e) {
			System.out.println("No Open Cart Button found");
		}

	}

}
