package com.webstaurantstore_page_factory.pages;

import java.time.Duration;
import java.util.List;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.NoSuchElementException;

import com.webstaurantstore_page_factory.utils.Utilities;

public class SearchPage extends Page {

	protected WebDriverWait wait;
	private final int DEFAULT_TIMEOUT = 10;

	@FindBy(css = "li.inline-block.leading-4.align-top.rounded-r-md > a")
	private WebElement nextPageButton;

	@FindBy(css = "#ProductBoxContainer" +
			":has(div.group.border-transparent.border-solid.border-6.m-0.max-w-full.relative.hover\\:outline-gray-200 > a > span)")
	private List<WebElement> products;

	@FindBy(css = "#ProductBoxContainer > div.group.border-transparent.border-solid.border-6.m-0.max-w-full.relative.hover\\:outline-gray-200 > a > span")
	private List<WebElement> productItems;

	@FindBy(name = "addToCartButton")
	private List<WebElement> addToCartButtons;

	@FindBy(xpath = "//h2[@class='notification__heading']")
	private WebElement viewCartAlert;

	@FindBy(xpath = "//div[@class='notification__description']")
	private WebElement itemAddedAlert;

	@FindBy(css = "a.btn.btn-small.btn-primary")
	private WebElement viewCartButton;

	public SearchPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, Duration.ofSeconds(DEFAULT_TIMEOUT));
	}

	public String getTitle(String titleStr){
		try {
			wait.until(ExpectedConditions.titleContains(titleStr));
			return driver.getTitle();
		} catch (NoSuchElementException e){
			return "";
		}
	}
	public boolean isLastPage() {
		final String NEXT_PAGE_BUTTON_LOCATOR_CSS_SELECTOR = "li.inline-block.leading-4.align-top.rounded-r-md > a";

		try {
			if (driver.getClass().getSimpleName().equals("FirefoxDriver") || driver.getClass().getSimpleName().equals("SafariDriver")) {
				nextPageButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(NEXT_PAGE_BUTTON_LOCATOR_CSS_SELECTOR)));
			}

			assert nextPageButton != null;
			if (nextPageButton.getAttribute("href") != null) {
				int pageNumber = Utilities.getPageNumber(nextPageButton.getAttribute("href"));
				if (pageNumber > -1) {
					nextPageButton.click();
					return false;
				}
			}

		} catch (NoSuchElementException e) {
			// Handle the case where the "next page" button is not found
			return true; // Assuming if the button is not found, it's the last page
		}
		return true;
	}

	public ArrayList<String> searchResultByPage() throws NoSuchElementException {
		ArrayList<String> searchResults = new ArrayList<>();

		if (!productItems.isEmpty()) {
			for (WebElement element : productItems) {
				searchResults.add(element.getText());
			}
		}
		return searchResults;
	}

	public void goToLastItemsPage(){
		// Test navigating to the last page of search results
		final String NEXT_PAGE_BUTTON_LOCATOR_CSS_SELECTOR = "li.inline-block.leading-4.align-top.rounded-r-md > a";

		int nextPage = 1;


		while (nextPage > -1) {
			try {

				if (driver.getClass().getSimpleName().equals("FirefoxDriver") || driver.getClass().getSimpleName().equals("SafariDriver")) {
					WebElement nextPageButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(NEXT_PAGE_BUTTON_LOCATOR_CSS_SELECTOR)));
				}

				assert nextPageButton != null;
				if (nextPageButton.getAttribute("href") != null) {
					nextPage = Utilities.getPageNumber(nextPageButton.getAttribute("href"));
					nextPageButton.click();
				}
			} catch (NoSuchElementException e) {
				System.out.println("Reached last page.");
				break;
			}
		}
	}

	public String addLastItemToCart() {
		final String PRODUCTS_LOCATOR_BY_CSS_SELECTOR = "#ProductBoxContainer:has(div.add-to-cart > form > div > div > input.btn.btn-cart.btn-small";
		final String PRODUCT_ITEMS_LOCATOR_BY_CSS_SELECTOR = "div.group.border-transparent.border-solid.border-6.m-0.max-w-full.relative.hover\\:outline-gray-200 > a > span";
		final String ADD_TO_CART_BUTTON_LOCATOR_BY_NAME = "addToCartButton";

		String itemAdded = "";
		WebElement addToCartButton = null;
		WebElement item = null;

		try {

			switch (driver.getClass().getSimpleName()) {
				case "ChromeDriver", "EdgeDriver" -> {
					if (products.isEmpty()) {
						return null; // Indicate no products found
					}

					WebElement lastProduct = products.get(products.size() - 1);

					addToCartButton = lastProduct.findElement(By.name(ADD_TO_CART_BUTTON_LOCATOR_BY_NAME));
					item = lastProduct.findElement(By.cssSelector(PRODUCT_ITEMS_LOCATOR_BY_CSS_SELECTOR));

				}
				case "FirefoxDriver", "SafariDriver" -> {
					List<WebElement> products = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy((By.cssSelector(PRODUCTS_LOCATOR_BY_CSS_SELECTOR))));

					if (products.isEmpty()) {
						return null; // Indicate no products found
					}

					WebElement lastProduct = products.get(products.size() - 1);
					addToCartButton = wait.until(ExpectedConditions.elementToBeClickable(lastProduct.findElement(By.name(ADD_TO_CART_BUTTON_LOCATOR_BY_NAME))));
					item = wait.until(ExpectedConditions.visibilityOf(lastProduct.findElement(By.cssSelector(PRODUCT_ITEMS_LOCATOR_BY_CSS_SELECTOR))));

				}
			}

			assert item != null;
			itemAdded = item.getText();
			addToCartButton.click();
			return itemAdded;

		}
		catch (NoSuchElementException e) {
			System.err.println("Can't add last item to cart");
		}
		return itemAdded;
	}

	public String addFirstItemToCart() {
		String itemAdded = "";

		try {
			final String ADD_TO_CART_BUTTONS_LOCATOR_BY_NAME = "addToCartButton";
			final String PRODUCT_ITEMS_LOCATOR_BY_CSS_SELECTOR = "div.group.border-transparent.border-solid.border-6.m-0.max-w-full.relative.hover\\:outline-gray-200 > a > span";

			if (driver.getClass().getSimpleName().equals("FirefoxDriver") || driver.getClass().getSimpleName().equals("SafariDriver")) {
				addToCartButtons = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name(ADD_TO_CART_BUTTONS_LOCATOR_BY_NAME)));
				productItems = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(PRODUCT_ITEMS_LOCATOR_BY_CSS_SELECTOR)));
			}

			if (addToCartButtons != null && productItems != null) {
				itemAdded = productItems.get(0).getText();
				addToCartButtons.get(0).click();
			}
		}
		catch (NoSuchElementException e) {
			System.err.println("Can't add last item to cart");
		}
		return itemAdded;
	}

	public String viewCartAlert() {
		try {

			wait.until(ExpectedConditions.visibilityOf(viewCartAlert));
			return viewCartAlert.getText();

		} catch (NoSuchElementException e) {
			System.out.println("View cart alert not found.");
			return "";
		}
	}

	public String getItemAddedAlert() {
		try {

			wait.until(ExpectedConditions.visibilityOf(itemAddedAlert));
			return itemAddedAlert.getText();

		} catch (NoSuchElementException e) {
			System.out.println("Item added message not found.");
			return "";
		}
	}


	public void submitViewCartButton() {
		final String WRONG_TITLE = "Stainless Work Table - WebstaurantStore";

		wait.until(ExpectedConditions.visibilityOf(viewCartButton));
		viewCartButton.click();

		// Validate second click (optional): sometimes the item adding confirmation alert is displayed twice
		// If the title is equal to self.WRONG_TITLE then a second alert is displayed
		if (this.getTitle().startsWith(WRONG_TITLE))
			viewCartButton.click();
	}
}